﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace PacManGame
{
    public class Entity : MovableObject
    {
        private Model model;
        private BoundingSphere modelBoundingSphere;
        private string name;
        private bool visible = true;
        private Color color = Color.White;

        public Entity(Model model, string name, bool transparent) : base()
        {
            this.model = model;
            this.name = name;
            calcBoundingSphere();
            this.Transparent = transparent;
        }

        public BoundingSphere ModelBoundingSphere
        {
            get { return modelBoundingSphere.Transform(DerivedMatrix); }
        }

        public Model Model
        {
            get { return model; }
            set { model = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public bool Visible
        {
            get { return visible; }
            set { visible = value; }
        }

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

        public void calcBoundingSphere()
        {
            foreach (ModelMesh m in Model.Meshes)
            {
                BoundingSphere meshBoundingSphere = m.BoundingSphere;
                meshBoundingSphere = meshBoundingSphere.Transform(m.ParentBone.Transform);
                BoundingSphere.CreateMerged(ref modelBoundingSphere, ref meshBoundingSphere, out modelBoundingSphere);
            }
        }

        public override void UpdateDerivedMatrix()
        {
            base.CalculateDerivedMatrix();

            this.NeedsUpdate = false;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Entity p = obj as Entity;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return name.Equals(p.Name);
        }

        public override int GetHashCode()
        {
            return name.GetHashCode();
        }
    }
}
