float4x4 World;
float4x4 View;
float4x4 Projection;
float4x4 InverseWorld;
float3 Color;
float3 LightDirection;
float AmbientIntensity;

struct VertexToPixel
{
    float4 Position     : POSITION;
    float3 Color        : COLOR0;
    float3 Light		: TEXCOORD0; 
    float3 Normal		: TEXCOORD1;

};

VertexToPixel VertexShader(float4 inPos : POSITION, float3 inNormal: NORMAL0)
{
    VertexToPixel Output = (VertexToPixel)0;

	float4x4 wvp = mul(mul(World, View), Projection);
    Output.Position = mul(inPos, wvp);
    Output.Color = Color;
    Output.Light =  normalize(LightDirection);
    Output.Normal = normalize(mul(inNormal, World));
    
    return Output;
}

float4 PixelShader(VertexToPixel input) : COLOR
{
    return float4(AmbientIntensity * input.Color + input.Color * saturate(dot(input.Light, input.Normal)), 0.5f);
}

technique GhostLighting
{
    pass Pass0
    {
         VertexShader = compile vs_2_0 VertexShader();
         PixelShader = compile ps_2_0 PixelShader();
    }
}