﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace PacManGame
{
    public abstract class MovableObject
    {
        protected Vector3 position;
        protected Vector3 scale;
        protected Quaternion rotation;
        protected Matrix derivedMatrix;
        private bool transparent;
        private bool needsUpdate;

        protected MovableObject()
        {
            derivedMatrix = Matrix.Identity;
            rotation = Quaternion.Identity;
            scale = Vector3.One;
            position = Vector3.Zero;
            transparent = false;
            needsUpdate = true;
        }

        protected MovableObject(bool transparent)
        {
            derivedMatrix = Matrix.Identity;
            rotation = Quaternion.Identity;
            scale = Vector3.One;
            position = Vector3.Zero;
            this.transparent = transparent;
        }

        public bool Transparent
        {
            get { return transparent; }
            set { transparent = value; }
        }

        public bool NeedsUpdate
        {
            get { return needsUpdate; }
            set { needsUpdate = value; }
        }

        public abstract void UpdateDerivedMatrix();

        protected void CalculateDerivedMatrix()
        {
            derivedMatrix = Matrix.CreateScale(scale) *
                 Matrix.CreateFromQuaternion(rotation) *
                 Matrix.CreateTranslation(position);
        }

        public Vector3 Scale
        {
            get { return scale; }
            set 
            {
                if (this.scale != value)
                {
                    scale = value;
                    this.needsUpdate = true;
                }
            }
        }

        public Quaternion Rotation
        {
            get { return rotation; }
            set
            {
                if (this.rotation != value)
                {
                    rotation = value;
                    this.needsUpdate = true;
                }
            }
        }

        public Vector3 Position
        {
            get { return position; }
            set 
            {
                if (this.position != value)
                {
                    position = value;
                    if (position.X > 25.5f)
                        position.X -= 26.0f;
                    else if (position.X < -0.5f)
                        position.X += 26.0f;
                    this.needsUpdate = true;
                }
            }
        }

        public Matrix DerivedMatrix
        {
            get { return derivedMatrix; }
        }
    }
}
