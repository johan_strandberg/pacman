﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Threading;

namespace PacManGame
{
    public enum Tile
    {
        Empty = 0,
        Wall,
        Dot,
        Cherry
    }

    public enum Direction
    {
        None = 0,
        Left,
        Right,
        Up,
        Down
    }

    public class SceneManager
    {
        Tile[][] level;
        List<Entity> entityList = new List<Entity>();
        List<Enemy> enemies = new List<Enemy>();
        private static volatile SceneManager sceneManager;
        private static object syncRoot = new object();
        Player player;
        Model wallModel_normal;
        Model wallModel_bend;
        Model box;
        Model apple;
        Entity pacModel;
        Entity powerupModel;
        Direction nextDirection = Direction.None;
        int totalDots = 0;
        int ghostScore = 200;
        int levelNumber = 1;
        float fruitactive = 0.0f;

        private SceneManager()
        {
        }

        public static SceneManager Instance
        {
            get
            {
                if (sceneManager == null)
                {
                    lock (syncRoot)
                    {
                        if (sceneManager == null)
                            sceneManager = new SceneManager();
                    }
                }
                return sceneManager;
            }
        }

        public Player Player
        {
            get { return player; }
            set { player = value; }
        }

        public bool GameFinished
        {
            get { return totalDots == 0; }
        }

        public int LevelNumber
        {
            get { return levelNumber; }
            set { levelNumber = value; }
        }

        public float Fruitactive
        {
            get { return fruitactive; }
            set { fruitactive = value; }
        }

        public void Initialize()
        {
            entityList.Clear();
            enemies.Clear();

            LoadLevel("level1.pac");
            Entity pacMan = new Entity(ResourceLoader.Instance.LoadModel("pacman"), "player", false);
            entityList.Add(pacMan);
            player = new Player();
            player.ControlledObject = pacMan;
            player.ControlledObject.Color = Color.Yellow;

            Model ghost = ResourceLoader.Instance.LoadModel("ghost");
            enemies.Add(CreateGhost("blinky", Color.Red));
            enemies.Add(CreateGhost("inky", Color.Aqua));
            enemies.Add(CreateGhost("pinky", Color.Pink));
            enemies.Add(CreateGhost("clyde", Color.Orange));

            ResetPositions();
        }

        private Enemy CreateGhost(string name, Color color) {
          Model ghost = ResourceLoader.Instance.LoadModel("ghost");
          Entity g = new Entity(ghost, name, true);
          e = new Enemy();
          e.ControlledObject = g;
          e.DefaultColor = color;
          return e;
        }

        public void ResetPositions()
        {
            player.ControlledObject.Position = new Vector3(12.0f, 0.25f, 22.0f);
            player.ControlledObject.UpdateDerivedMatrix();

            int i = 0;

            foreach (Enemy e in enemies)
            {
                e.ControlledObject.Position = new Vector3(11.0f + i, 0.0f, 14.0f);
                e.ControlledObject.UpdateDerivedMatrix();
                e.Frightened = false;
                e.Eaten = false;
                i++;
            }
        }

        public void LoadContent()
        {
            wallModel_normal = ResourceLoader.Instance.LoadModel("wall_normal");
            wallModel_bend = ResourceLoader.Instance.LoadModel("wall_bend");
            apple = ResourceLoader.Instance.LoadModel("apple");
            box = ResourceLoader.Instance.LoadModel("box");
            pacModel = new Entity(ResourceLoader.Instance.LoadModel("pac"), "pac", false);
            pacModel.Color = Color.Yellow;
            pacModel.Scale = new Vector3(0.2f);

            powerupModel = new Entity(ResourceLoader.Instance.LoadModel("pac"), "powerup", false);
            powerupModel.Color = Color.LightBlue;
            powerupModel.Scale = new Vector3(0.8f);
        }

        public void Update(GameTime gameTime)
        {
            powerupModel.Scale = new Vector3(0.75f + (float)Math.Sin(gameTime.TotalGameTime.TotalSeconds * 8) / 4.0f);

            Enemy collidingEnemy = CheckEnemyCollision(player);
            if (collidingEnemy != null)
            {
                if (collidingEnemy.Frightened)
                {
                    collidingEnemy.Frightened = false;
                    collidingEnemy.ControlledObject.Position = new Vector3(11.0f, 0.0f, 14.0f);
                    collidingEnemy.ControlledObject.UpdateDerivedMatrix();
                    SoundManager.Instance.PlaySound("kill");
                    player.Score += ghostScore;
                    ghostScore *= 2;
                }
                else if (!collidingEnemy.Eaten)
                {
                    player.IsDead = true;
                    player.Lives--;
                }
            }

            UpdatePlayerMovement(gameTime);

            if (player.IsMoving && player.MoveDirection != Vector2.Zero)
            {
                if (player.AnimateUp)
                {
                    player.AnimationAngle += (float)(gameTime.ElapsedGameTime.TotalSeconds * player.MoveSpeed);
                    if (player.AnimationAngle >= Math.PI / 4)
                        player.AnimateUp = false;
                }
                else
                {
                    player.AnimationAngle -= (float)(gameTime.ElapsedGameTime.TotalSeconds * player.MoveSpeed);
                    if (player.AnimationAngle <= 0)
                        player.AnimateUp = true;
                }
            }

            foreach (Enemy e in enemies)
            {
                e.Update(gameTime);
                UpdateEnemyMovement(e, gameTime);
            }

            foreach (Entity e in entityList)
            {
                if (e.NeedsUpdate)
                    e.UpdateDerivedMatrix();
            }

            if (fruitactive > 0.0f)
            {
                if ((player.ControlledObject.Position - new Vector3(12.5f, 0.5f, 16.0f)).Length() < 0.8f)
                {
                    fruitactive = 0.0f;
                    player.Score += 100;
                }
                fruitactive -= (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
        }

        public void Draw(GameTime gameTime)
        {
            Matrix m = Matrix.Identity;

            for (int i = -1; i <= level.Length; ++i)
            {
                for (int j = -1; j <= level[0].Length; ++j)
                {
                    if (i < 0 || j < 0 || i == level.Length || j == level[i].Length)
                    {
                        m.Translation = new Vector3(j, 0, i);
                        if(i != 13)
                            GraphicsManager.Instance.DrawModel(box, m, new Color((byte)0, (byte)97, (byte)173));
                    }
                    else if (level[i][j] == Tile.Wall)
                    {
                        Quaternion rotation;
                        Model wall = this.PickWallModel(j, i, out rotation);
                        Matrix.CreateFromQuaternion(ref rotation, out m);
                        m.Translation = new Vector3(j, 0.25f, i);
                        if (wall != null)
                            GraphicsManager.Instance.DrawModel(wall, m);
                    }
                    else if (level[i][j] == Tile.Dot)
                    {
                        pacModel.Position = new Vector3(j, 0.0f, i);
                        pacModel.UpdateDerivedMatrix();
                        GraphicsManager.Instance.DrawEntity(pacModel);
                    }
                    else if (level[i][j] == Tile.Cherry)
                    {
                        powerupModel.Position = new Vector3(j, 0.0f, i);
                        powerupModel.UpdateDerivedMatrix();
                        GraphicsManager.Instance.DrawEntity(powerupModel);
                    }
                }
            }

            foreach (Entity e in entityList)
            {
                GraphicsManager.Instance.DrawEntity(e);
            }

            if (fruitactive > 0.0f)
            {
                m.Translation = new Vector3(12.5f, 0.5f, 16.0f);
                GraphicsManager.Instance.DrawModel(apple, m);
            }

            player.Draw();
        }

        private void UpdatePlayerMovement(GameTime gameTime)
        {
            int player_x = (int)Math.Round(player.ControlledObject.Position.X, 0, MidpointRounding.ToEven);
            int player_z = (int)Math.Round(player.ControlledObject.Position.Z, 0, MidpointRounding.ToEven);

            if (Math.Abs(player.ControlledObject.Position.Z - player_z) < 0.05f)
            {
                if (nextDirection == Direction.Left &&
                    !(Math.Abs(player.ControlledObject.Position.X - player_x) < 0.05f && CheckWallCollision(player_x - 1, player_z)))
                {
                    player.MoveLeft();
                    nextDirection = Direction.None;
                }
                else if (nextDirection == Direction.Right &&
                    !(Math.Abs(player.ControlledObject.Position.X - player_x) < 0.05f && CheckWallCollision(player_x + 1, player_z)))
                {
                    player.MoveRight();
                    nextDirection = Direction.None;
                }
            }
            if (Math.Abs(player.ControlledObject.Position.X - player_x) < 0.05f)
            {
                if (nextDirection == Direction.Up &&
                    !(Math.Abs(player.ControlledObject.Position.Z - player_z) < 0.05f && CheckWallCollision(player_x, player_z - 1)))
                {
                    player.MoveForward();
                    nextDirection = Direction.None;
                }
                else if (nextDirection == Direction.Down &&
                    !(Math.Abs(player.ControlledObject.Position.Z - player_z) < 0.05f && CheckWallCollision(player_x, player_z + 1)))
                {
                    player.MoveBackward();
                    nextDirection = Direction.None;
                }
            }

            if (player.ControlledObject != null)
            {
                Vector3 dv = new Vector3(player.MoveDirection.X * player.MoveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds, 0, player.MoveDirection.Y * player.MoveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds);
                if (Math.Abs(player.ControlledObject.Position.X - player_x) < 0.05f &&
                    Math.Abs(player.ControlledObject.Position.Z - player_z) < 0.05f)
                {
                    CheckDotCollision(player_x, player_z);

                    int dx = dv.X != 0 ? (int)(dv.X / Math.Abs(dv.X)) : 0;
                    int dz = dv.Z != 0 ? (int)(dv.Z / Math.Abs(dv.Z)) : 0;
                    if (!CheckWallCollision(player_x + dx, player_z + dz))
                    {
                        player.ControlledObject.Position += dv;
                        player.IsMoving = true;
                    }
                    else
                        player.IsMoving = false;
                }
                else
                {
                    player.ControlledObject.Position += dv;
                    player.IsMoving = true;
                }
            }
        }

        private void UpdateEnemyMovement(Enemy e, GameTime gameTime)
        {
            int player_x = (int)Math.Round(e.ControlledObject.Position.X, 0, MidpointRounding.ToEven);
            int player_z = (int)Math.Round(e.ControlledObject.Position.Z, 0, MidpointRounding.ToEven);

            if ((e.LastJunction.X != player_x || e.LastJunction.Y != player_z) || !e.IsMoving)
            {
                if (Math.Abs(e.ControlledObject.Position.Z - player_z) < 0.05f &&
                    Math.Abs(e.ControlledObject.Position.X - player_x) < 0.05f)
                {
                    e.UpdateDirection(player_x, player_z);
                }
            }

            if (e.ControlledObject != null)
            {
                Vector3 dv = new Vector3(e.MoveDirection.X * e.MoveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds, 0, e.MoveDirection.Y * e.MoveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds);
                if (Math.Abs(e.ControlledObject.Position.X - player_x) < 0.05f &&
                    Math.Abs(e.ControlledObject.Position.Z - player_z) < 0.05f)
                {
                    int dx = dv.X != 0 ? (int)(dv.X / Math.Abs(dv.X)) : 0;
                    int dz = dv.Z != 0 ? (int)(dv.Z / Math.Abs(dv.Z)) : 0;
                    if (!CheckWallCollision(player_x + dx, player_z + dz))
                    {
                        e.ControlledObject.Position += dv;
                        e.IsMoving = true;
                    }
                    else
                    {
                        e.IsMoving = false;
                    }
                }
                else
                {
                    e.ControlledObject.Position += dv;
                    e.IsMoving = true;
                }
            }
        }

        private Enemy CheckEnemyCollision(Player p)
        {
            foreach (Enemy e in enemies)
            {
                if ((e.ControlledObject.Position - p.ControlledObject.Position).Length() < 0.8f)
                    return e;
            }

            return null;
        }

        public bool CheckWallCollision(int x, int y)
        {
            if (y == 13 && x == level[y].Length)
                return false;
            else if (y == 13 && x == -1)
                return false;
            else if (y < level.Length && y >= 0)
                if (x < level[y].Length && x >= 0)
                    return level[y][x] == Tile.Wall;
            return true;
        }

        private void CheckDotCollision(int x, int y)
        {
            if (y < level.Length && y >= 0)
                if (x < level[y].Length && x >= 0)
                    if (level[y][x] == Tile.Dot)
                    {
                        player.Score += 10;
                        level[y][x] = Tile.Empty;
                        totalDots--;
                        SoundManager.Instance.PlaySound("point");
                    }
                    else if (level[y][x] == Tile.Cherry)
                    {
                        player.Score += 50;
                        level[y][x] = Tile.Empty;
                        totalDots--;
                        foreach (Enemy e in enemies)
                        {
                            e.Frightened = true;
                        }
                        ghostScore = 200;
                        SoundManager.Instance.PlaySound("powerup");
                    }
        }

        private void LoadLevel(string file)
        {
            Texture2D image = ResourceLoader.Instance.loadTexture2D("level1");
            Color[] pixels = new Color[image.Width * image.Height];
            level = new Tile[image.Height][];
            image.GetData<Color>(pixels);
            totalDots = 0;

            for (int i = 0; i < image.Height; i++)
            {
                level[i] = new Tile[image.Width];
                for (int j = 0; j < image.Width; j++)
                {
                    if (pixels[i * image.Width + j] == Color.Black)
                        level[i][j] = Tile.Wall;
                    else if (pixels[i * image.Width + j] == Color.White)
                        level[i][j] = Tile.Empty;
                    else if (pixels[i * image.Width + j] == Color.Lime)
                    {
                        level[i][j] = Tile.Dot;
                        totalDots++;
                    }
                    else if (pixels[i * image.Width + j] == Color.Red)
                    {
                        level[i][j] = Tile.Cherry;
                        totalDots++;
                    }
                }
            }
        }

        public void MovePlayer(Direction dir)
        {
            nextDirection = dir;
        }

        private Model PickWallModel(int x, int y, out Quaternion rotation)
        {
            if (CheckWallCollision(x - 1, y) && CheckWallCollision(x + 1, y) &&
                CheckWallCollision(x, y - 1) && CheckWallCollision(x, y + 1))
            {
                if (!CheckWallCollision(x + 1, y + 1))
                {
                    rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, (float)(Math.PI / 2.0));
                    return wallModel_bend;
                }
                else if (!CheckWallCollision(x - 1, y + 1))
                {
                    rotation = Quaternion.Identity;
                    return wallModel_bend;
                }
                else if (!CheckWallCollision(x - 1, y - 1))
                {
                    rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, -(float)(Math.PI / 2.0));
                    return wallModel_bend;
                }
                else if (!CheckWallCollision(x + 1, y - 1))
                {
                    rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, (float)Math.PI);
                    return wallModel_bend;
                }
                else
                {
                    rotation = Quaternion.Identity;
                    return null;
                }
            }
            else if (CheckWallCollision(x - 1, y) && CheckWallCollision(x + 1, y))
            {
                rotation = Quaternion.Identity;
                return wallModel_normal;
            }
            else if (CheckWallCollision(x, y - 1) && CheckWallCollision(x, y + 1))
            {
                rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, (float)(Math.PI / 2.0));
                return wallModel_normal;
            }
            else if (CheckWallCollision(x + 1, y) && CheckWallCollision(x, y + 1))
            {
                rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, (float)(Math.PI / 2.0));
                return wallModel_bend;
            }
            else if (CheckWallCollision(x - 1, y) && CheckWallCollision(x, y + 1))
            {
                rotation = Quaternion.Identity;
                return wallModel_bend;
            }
            else if (CheckWallCollision(x - 1, y) && CheckWallCollision(x, y - 1))
            {
                rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, -(float)(Math.PI / 2.0));
                return wallModel_bend;
            }
            else if (CheckWallCollision(x + 1, y) && CheckWallCollision(x, y - 1))
            {
                rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, (float)Math.PI);
                return wallModel_bend;
            }
            else
            {
                rotation = Quaternion.Identity;
                return null;
            }
        }

        public Direction Opposite(Direction direction)
        {
            if (direction == Direction.Down)
                return Direction.Up;
            else if (direction == Direction.Up)
                return Direction.Down;
            else if (direction == Direction.Left)
                return Direction.Right;
            else if (direction == Direction.Right)
                return Direction.Left;
            else
                return Direction.None;
        }

        public void NextLevel()
        {
            ResetPositions();
            LoadLevel("level1");
            player.Score += 1000;
            levelNumber++;
        }
    }
}
