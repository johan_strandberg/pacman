﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace PacManGame
{
    public class SoundManager
    {
        private static volatile SoundManager soundManager;
        private static object syncRoot = new object();

        Song music;
        Dictionary<string, SoundEffect> sounds = new Dictionary<string,SoundEffect>();

        private SoundManager()
        {
        }

        public static SoundManager Instance
        {
            get
            {
                if (soundManager == null)
                {
                    lock (syncRoot)
                    {
                        if (soundManager == null)
                            soundManager = new SoundManager();
                    }
                }
                return soundManager;
            }
        }

        public void Initialize()
        {
            music = ResourceLoader.Instance.loadSong("sounds/bgm");
            sounds.Add("point", ResourceLoader.Instance.loadSound("sounds/nsmb_coin"));
            sounds.Add("powerup", ResourceLoader.Instance.loadSound("sounds/nsmb_power-up"));
            sounds.Add("kill", ResourceLoader.Instance.loadSound("sounds/nsmb_fireball"));
        }

        public void PlayMusic()
        {
            MediaPlayer.Play(music);
            MediaPlayer.Volume = 0.2f;
        }

        public void PlaySound(string name)
        {
            sounds[name].Play();
        }

        public void StopMusic()
        {
            MediaPlayer.Stop();
        }

        public void PauseMusic()
        {
            MediaPlayer.Pause();
        }
    }
}
