﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace PacManGame
{
    public class ResourceLoader
    {
        private Game game;
        private static volatile ResourceLoader resourceLoader;
        private static object syncRoot = new object();
        private Dictionary<String, Effect> effects;
        private Dictionary<String, Texture2D> textures;

        private ResourceLoader()
        {
        }

        public static ResourceLoader Instance
        {
            get
            {
                if (resourceLoader == null)
                {
                    lock (syncRoot)
                    {
                        if (resourceLoader == null)
                            resourceLoader = new ResourceLoader();
                    }
                }
                return resourceLoader;
            }
        }

        public Dictionary<String, Effect> Effects
        {
            get { return effects; }
            set { effects = value; }
        }

        public Dictionary<String, Texture2D> Textures
        {
            get { return textures; }
            set { textures = value; }
        }

        public ContentManager ContentManager
        {
            get { return game.Content; }
        }

        public void Initialize(Game game)
        {
            this.game = game;
            effects = new Dictionary<string, Effect>();
            textures = new Dictionary<string, Texture2D>();
        }

        public Model LoadModel(string file)
        {
            Model model = game.Content.Load<Model>(@file);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.Parameters["AmbientLightColor"].SetValue(new Vector3(Settings.AMBIENT_INTENSITY));
                    effect.DirectionalLight0.Direction = Settings.LIGHT_DIRECTION;
                    effect.DirectionalLight0.Enabled = true;
                    effect.DirectionalLight0.DiffuseColor = Vector3.One;
                    effect.DirectionalLight0.SpecularColor = Vector3.One;
                    effect.DirectionalLight1.Enabled = false;
                    effect.DirectionalLight2.Enabled = false;
                    effect.PreferPerPixelLighting = true;
                }
            }

            return model;
        }

        public Model LoadModel(string file, string effect)
        {
            Model model = game.Content.Load<Model>(@file);
            Effect e;
            loadEffect(effect, out e);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart meshPart in mesh.MeshParts)
                    meshPart.Effect = e.Clone(game.GraphicsDevice);
            }

            return model;
        }

        public void loadEffect(String name, out Effect effect)
        {
            if (!effects.ContainsKey(name))
                effects.Add(name, game.Content.Load<Effect>(name));
            
            effect = effects[name];
        }

        public void loadTexture2D(String name, out Texture2D texture)
        {
            if (!textures.ContainsKey(name))
                textures.Add(name, game.Content.Load<Texture2D>(name));
            
            texture = textures[name];
        }

        public Texture2D loadTexture2D(String name)
        {
            if (!textures.ContainsKey(name))
                textures.Add(name, game.Content.Load<Texture2D>(name));

            return textures[name];
        }

        public SpriteFont loadFont(String name)
        {
            return game.Content.Load<SpriteFont>(name);
        }

        public SoundEffect loadSound(String name)
        {
            return game.Content.Load<SoundEffect>(name);
        }

        public Song loadSong(String name)
        {
            return game.Content.Load<Song>(name);
        }
    }
}
