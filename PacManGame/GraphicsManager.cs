﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PacManGame.Graphics;

namespace PacManGame
{
    class GraphicsManager
    {
        private static volatile GraphicsManager graphicsManager;
        private static object syncRoot = new object();
        private Camera currentCamera;
        private GraphicsDeviceManager graphicsDeviceManager;
        private List<Entity> transparentObjects = new List<Entity>();

        private GraphicsManager()
        {

        }

        public static GraphicsManager Instance
        {
            get
            {
                if (graphicsManager == null)
                {
                    lock (syncRoot)
                    {
                        if (graphicsManager == null)
                        {
                            graphicsManager = new GraphicsManager();
                        }
                    }
                }

                return graphicsManager;
            }
        }

        public Camera CurrentCamera
        {
            get { return currentCamera; }
            set { currentCamera = value; }
        }

        public void Initialize(Game game)
        {
            graphicsDeviceManager = new GraphicsDeviceManager(game);
            graphicsDeviceManager.PreferredBackBufferWidth = 600;
            graphicsDeviceManager.PreferredBackBufferHeight = 600;
            graphicsDeviceManager.IsFullScreen = false;
            graphicsDeviceManager.ApplyChanges();

            graphicsDeviceManager.GraphicsDevice.RenderState.DepthBufferEnable = true;
            graphicsDeviceManager.GraphicsDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;

            currentCamera = new Camera(graphicsDeviceManager.GraphicsDevice.Viewport,
                                       new Vector3(12.5f, 40.0f, 36.0f),
                                       new Vector3(12.5f, 0.0f, 14.0f),
                                       Vector3.Forward);
        }

        public void Begin()
        {
            graphicsDeviceManager.GraphicsDevice.RenderState.DepthBufferEnable = true;
            graphicsDeviceManager.GraphicsDevice.RenderState.DepthBufferWriteEnable = true;
            currentCamera.Update();
        }

        public void DrawModel(Model model, Matrix matrix)
        {
            Matrix[] boneTransforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(boneTransforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.Parameters["World"].SetValue(boneTransforms[mesh.ParentBone.Index] * matrix);
                    effect.Parameters["View"].SetValue(currentCamera.ViewMatrix);
                    effect.Parameters["Projection"].SetValue(currentCamera.ProjectionMatrix);
                }
                mesh.Draw();
            }
        }

        public void DrawModel(Model model, Matrix matrix, Color color)
        {
            Matrix[] boneTransforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(boneTransforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.AmbientLightColor = (color.ToVector3() * Settings.AMBIENT_INTENSITY);
                    effect.DiffuseColor = color.ToVector3();
                    effect.Parameters["World"].SetValue(boneTransforms[mesh.ParentBone.Index] * matrix);
                    effect.Parameters["View"].SetValue(currentCamera.ViewMatrix);
                    effect.Parameters["Projection"].SetValue(currentCamera.ProjectionMatrix);
                }
                mesh.Draw();
            }
        }

        public void DrawEntity(Entity e)
        {
            if (e.Transparent)
            {
                transparentObjects.Add(e);
                return;
            }

            Matrix[] boneTransforms = new Matrix[e.Model.Bones.Count];
            e.Model.CopyAbsoluteBoneTransformsTo(boneTransforms);
            
            foreach (ModelMesh mesh in e.Model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.DiffuseColor = e.Color.ToVector3();
                    effect.AmbientLightColor = (e.Color.ToVector3() * Settings.AMBIENT_INTENSITY);
                    effect.Parameters["World"].SetValue(boneTransforms[mesh.ParentBone.Index] * e.DerivedMatrix);
                    effect.Parameters["View"].SetValue(currentCamera.ViewMatrix);
                    effect.Parameters["Projection"].SetValue(currentCamera.ProjectionMatrix);
                }
                mesh.Draw();
            }
        }

        private void DrawTransparentEntity(Entity e)
        {
            Matrix[] boneTransforms = new Matrix[e.Model.Bones.Count];
            e.Model.CopyAbsoluteBoneTransformsTo(boneTransforms);

            foreach (ModelMesh mesh in e.Model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.AmbientLightColor = (e.Color.ToVector3() * Settings.AMBIENT_INTENSITY);
                    effect.DiffuseColor = e.Color.ToVector3();
                    effect.Parameters["World"].SetValue(boneTransforms[mesh.ParentBone.Index] * e.DerivedMatrix);
                    effect.Parameters["View"].SetValue(currentCamera.ViewMatrix);
                    effect.Parameters["Projection"].SetValue(currentCamera.ProjectionMatrix);
                }
                mesh.Draw();
            }
        }

        public void Finish()
        {
            if (transparentObjects.Count > 0)
            {
                graphicsDeviceManager.GraphicsDevice.RenderState.CullMode = CullMode.None;
                graphicsDeviceManager.GraphicsDevice.RenderState.AlphaBlendEnable = true;
                graphicsDeviceManager.GraphicsDevice.RenderState.AlphaTestEnable = true;
                graphicsDeviceManager.GraphicsDevice.RenderState.DestinationBlend = Blend.InverseSourceAlpha;

                foreach (Entity e in transparentObjects)
                {
                    DrawTransparentEntity(e);
                }
                graphicsDeviceManager.GraphicsDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
            }

            
            transparentObjects.Clear();
        }
    }
}
