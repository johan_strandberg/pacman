using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using PacManGame.Input;
using BloomPostprocess;
using PacManGame.Graphics;

namespace PacManGame
{
    /// <summary>
    /// This game is created by:
    /// Name:               Johan Strandberg
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        SpriteBatch spriteBatch;
        SceneManager sceneManager;
        ScreenManager.ScreenManager screenManager;
        SpriteFont font1;

        public Game1()
        {
            Content.RootDirectory = "Content";
        }

        public SceneManager SceneManager
        {
            get { return sceneManager; }
            set { sceneManager = value; }
        }

        public SpriteBatch SpriteBatch
        {
            get { return spriteBatch; }
            set { spriteBatch = value; }
        }

        protected override void Initialize()
        {
            GraphicsManager.Instance.Initialize(this);
            Window.Title = "PacManGame";
            ResourceLoader.Instance.Initialize(this);
            SoundManager.Instance.Initialize();

            screenManager = new PacManGame.ScreenManager.ScreenManager(this);
            Components.Add(screenManager);

            screenManager.AddScreen(new Screens.TitleScreen(), null);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            font1 = ResourceLoader.Instance.loadFont("font1");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {

        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            base.Draw(gameTime);
            GraphicsDevice.Present();
        }
    }
}
