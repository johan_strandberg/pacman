﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PacManGame.Graphics
{
    public class ReflectionPlane
    {
        RenderTarget2D reflectionRenderTarget;
        RenderTarget2D blurRenderTarget1;
        RenderTarget2D blurRenderTarget2;
        Texture2D reflectionMap;
        Matrix reflectionViewMatrix;
        VertexBuffer vertexBuffer;
        VertexDeclaration vertexDeclaration;
        Effect effect;
        Effect blur;
        Game game;
        SpriteBatch spriteBatch;

        public ReflectionPlane(Game game)
        {
            this.game = game;
            spriteBatch = ((Game1)game).SpriteBatch;
            PresentationParameters pp = game.GraphicsDevice.PresentationParameters;
            reflectionRenderTarget = new RenderTarget2D(game.GraphicsDevice, pp.BackBufferWidth, pp.BackBufferHeight, 1, game.GraphicsDevice.DisplayMode.Format);
            blurRenderTarget1 = new RenderTarget2D(game.GraphicsDevice, pp.BackBufferWidth, pp.BackBufferHeight, 1, game.GraphicsDevice.DisplayMode.Format);
            blurRenderTarget2 = new RenderTarget2D(game.GraphicsDevice, pp.BackBufferWidth, pp.BackBufferHeight, 1, game.GraphicsDevice.DisplayMode.Format);
            SetUpVertices();
            vertexDeclaration = new VertexDeclaration(game.GraphicsDevice, VertexPositionTexture.VertexElements);
            ResourceLoader.Instance.loadEffect("fx/Reflection", out effect);
            ResourceLoader.Instance.loadEffect("fx/GaussianBlur", out blur);
        }

        private void SetUpVertices()
        {
            VertexPositionTexture[] vertices = new VertexPositionTexture[6];

            vertices[0] = new VertexPositionTexture(new Vector3(-1.5f, -0.5f, -1.5f), new Vector2(0, 0));
            vertices[1] = new VertexPositionTexture(new Vector3(26.5f, -0.5f, 29.5f), new Vector2(1, 1));
            vertices[2] = new VertexPositionTexture(new Vector3(-1.5f, -0.5f, 29.5f), new Vector2(0, 1));

            vertices[3] = new VertexPositionTexture(new Vector3(-1.5f, -0.5f, -1.5f), new Vector2(0, 0));
            vertices[4] = new VertexPositionTexture(new Vector3(26.5f, -0.5f, -1.5f), new Vector2(1, 0));
            vertices[5] = new VertexPositionTexture(new Vector3(26.5f, -0.5f, 29.5f), new Vector2(1, 1));

            vertexBuffer = new VertexBuffer(game.GraphicsDevice, vertices.Length * VertexPositionTexture.SizeInBytes, BufferUsage.WriteOnly);
            vertexBuffer.SetData(vertices);
        }

        private Plane CreatePlane(float height, Vector3 planeNormalDirection, Matrix currentViewMatrix, bool clipSide)
        {
            planeNormalDirection.Normalize();
            Vector4 planeCoeffs = new Vector4(planeNormalDirection, height);
            if (clipSide)
                planeCoeffs *= -1;

            Matrix worldViewProjection = currentViewMatrix * GraphicsManager.Instance.CurrentCamera.ProjectionMatrix;
            Matrix inverseWorldViewProjection = Matrix.Invert(worldViewProjection);
            inverseWorldViewProjection = Matrix.Transpose(inverseWorldViewProjection);

            planeCoeffs = Vector4.Transform(planeCoeffs, inverseWorldViewProjection);
            Plane finalPlane = new Plane(planeCoeffs);

            return finalPlane;
        }

        private void DrawReflectionMap(GameTime gameTime)
        {
            Vector3 cameraPosition = GraphicsManager.Instance.CurrentCamera.Position;

            Vector3 reflCameraPosition = cameraPosition;
            reflCameraPosition.Y = -cameraPosition.Y;
            Vector3 reflTargetPos = GraphicsManager.Instance.CurrentCamera.LookAt;
            reflTargetPos.Y = -reflTargetPos.Y;

            Vector3 invUpVector = -GraphicsManager.Instance.CurrentCamera.Up;

            reflectionViewMatrix = Matrix.CreateLookAt(reflCameraPosition, reflTargetPos, invUpVector);
            Matrix temp = GraphicsManager.Instance.CurrentCamera.ViewMatrix;

            Plane reflectionPlane = CreatePlane(-0.5f, new Vector3(0, -1, 0), reflectionViewMatrix, true);
            game.GraphicsDevice.ClipPlanes[0].Plane = reflectionPlane;
            game.GraphicsDevice.ClipPlanes[0].IsEnabled = true;
            game.GraphicsDevice.SetRenderTarget(0, reflectionRenderTarget);
            game.GraphicsDevice.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.TransparentBlack, 1.0f, 0);
            GraphicsManager.Instance.Begin();
            GraphicsManager.Instance.CurrentCamera.ViewMatrix = reflectionViewMatrix;
            SceneManager.Instance.Draw(gameTime);
            GraphicsManager.Instance.Finish();
            game.GraphicsDevice.ClipPlanes[0].IsEnabled = false;
            GraphicsManager.Instance.CurrentCamera.ViewMatrix = temp;

            game.GraphicsDevice.SetRenderTarget(0, null);
            
            reflectionMap = reflectionRenderTarget.GetTexture();

            ApplyGaussianBlur();
            game.GraphicsDevice.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.TransparentBlack, 1.0f, 0);
        }

        public void Draw(GameTime gameTime)
        {
            DrawReflectionMap(gameTime);
            
            effect.CurrentTechnique = effect.Techniques["Water"];
            Matrix worldMatrix = Matrix.Identity;
            effect.Parameters["xWorld"].SetValue(worldMatrix);
            effect.Parameters["xView"].SetValue(GraphicsManager.Instance.CurrentCamera.ViewMatrix);
            effect.Parameters["xReflectionView"].SetValue(reflectionViewMatrix);
            effect.Parameters["xProjection"].SetValue(GraphicsManager.Instance.CurrentCamera.ProjectionMatrix);
            effect.Parameters["xReflectionMap"].SetValue(blurRenderTarget2.GetTexture());

            effect.Begin();
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Begin();
                
                game.GraphicsDevice.Vertices[0].SetSource(vertexBuffer, 0, VertexPositionTexture.SizeInBytes);
                game.GraphicsDevice.VertexDeclaration = vertexDeclaration;
                int noVertices = vertexBuffer.SizeInBytes / VertexPositionTexture.SizeInBytes;
                game.GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, noVertices / 3);

                pass.End();
            }
            effect.End();
        }

        private void ApplyGaussianBlur()
        {
            SetBlurEffectParameters(1.0f / (float)blurRenderTarget1.Width, 0);

            DrawFullscreenQuad(reflectionMap, blurRenderTarget1,
                               blur);

            // Pass 3: draw from rendertarget 2 back into rendertarget 1,
            // using a shader to apply a vertical gaussian blur filter.
            SetBlurEffectParameters(0, 1.0f / (float)blurRenderTarget1.Height);

            DrawFullscreenQuad(blurRenderTarget1.GetTexture(), blurRenderTarget2,
                               blur);
        }

        void SetBlurEffectParameters(float dx, float dy)
        {
            // Look up the sample weight and offset effect parameters.
            EffectParameter weightsParameter, offsetsParameter;

            weightsParameter = blur.Parameters["SampleWeights"];
            offsetsParameter = blur.Parameters["SampleOffsets"];

            // Look up how many samples our gaussian blur effect supports.
            int sampleCount = weightsParameter.Elements.Count;

            // Create temporary arrays for computing our filter settings.
            float[] sampleWeights = new float[sampleCount];
            Vector2[] sampleOffsets = new Vector2[sampleCount];

            // The first sample always has a zero offset.
            sampleWeights[0] = ComputeGaussian(0);
            sampleOffsets[0] = new Vector2(0);

            // Maintain a sum of all the weighting values.
            float totalWeights = sampleWeights[0];

            // Add pairs of additional sample taps, positioned
            // along a line in both directions from the center.
            for (int i = 0; i < sampleCount / 2; i++)
            {
                // Store weights for the positive and negative taps.
                float weight = ComputeGaussian(i + 1);

                sampleWeights[i * 2 + 1] = weight;
                sampleWeights[i * 2 + 2] = weight;

                totalWeights += weight * 2;

                // To get the maximum amount of blurring from a limited number of
                // pixel shader samples, we take advantage of the bilinear filtering
                // hardware inside the texture fetch unit. If we position our texture
                // coordinates exactly halfway between two texels, the filtering unit
                // will average them for us, giving two samples for the price of one.
                // This allows us to step in units of two texels per sample, rather
                // than just one at a time. The 1.5 offset kicks things off by
                // positioning us nicely in between two texels.
                float sampleOffset = i * 2 + 1.5f;

                Vector2 delta = new Vector2(dx, dy) * sampleOffset;

                // Store texture coordinate offsets for the positive and negative taps.
                sampleOffsets[i * 2 + 1] = delta;
                sampleOffsets[i * 2 + 2] = -delta;
            }

            // Normalize the list of sample weightings, so they will always sum to one.
            for (int i = 0; i < sampleWeights.Length; i++)
            {
                sampleWeights[i] /= totalWeights;
            }

            // Tell the effect about our new filter settings.
            weightsParameter.SetValue(sampleWeights);
            offsetsParameter.SetValue(sampleOffsets);
        }


        /// <summary>
        /// Evaluates a single point on the gaussian falloff curve.
        /// Used for setting up the blur filter weightings.
        /// </summary>
        float ComputeGaussian(float n)
        {
            float theta = 2;

            return (float)((1.0 / Math.Sqrt(2 * Math.PI * theta)) *
                           Math.Exp(-(n * n) / (2 * theta * theta)));
        }

        void DrawFullscreenQuad(Texture2D texture, RenderTarget2D renderTarget,
                                Effect effect)
        {
            game.GraphicsDevice.SetRenderTarget(0, renderTarget);

            DrawFullscreenQuad(texture,
                               renderTarget.Width, renderTarget.Height,
                               effect);

            game.GraphicsDevice.SetRenderTarget(0, null);
        }


        /// <summary>
        /// Helper for drawing a texture into the current rendertarget,
        /// using a custom shader to apply postprocessing effects.
        /// </summary>
        void DrawFullscreenQuad(Texture2D texture, int width, int height,
                                Effect effect)
        {
            spriteBatch.Begin(SpriteBlendMode.None,
                              SpriteSortMode.Immediate,
                              SaveStateMode.None);

            effect.Begin();
            effect.CurrentTechnique.Passes[0].Begin();

            // Draw the quad.
            spriteBatch.Draw(texture, new Rectangle(0, 0, width, height), Color.White);
            spriteBatch.End();

            effect.CurrentTechnique.Passes[0].End();
            effect.End();
        }
    }
}
