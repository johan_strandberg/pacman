﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace PacManGame.Input
{
    public class Keyboard : IControl
    {
        #region IControl Members

        private KeyboardState oldState;
        private float moveSpeed;
        private Game game;

        public void Initialize(Game game)
        {
            oldState = Microsoft.Xna.Framework.Input.Keyboard.GetState();
            this.game = game;
        }

        public void Update(GameTime gameTime)
        {
            KeyboardState keyState = Microsoft.Xna.Framework.Input.Keyboard.GetState();
            if(keyState.IsKeyUp(Keys.Escape) && oldState.IsKeyDown(Keys.Escape))
            {
                game.Exit();
            }
            if (keyState.IsKeyDown(Keys.W))
            {
                if (Settings.USE_CHASECAM)
                {
                    (game as Game1).SceneManager.MovePlayer(Direction.Up);
                }
                else
                {
                    Vector3 pos = GraphicsManager.Instance.CurrentCamera.Position;
                    Vector3 lookAt = GraphicsManager.Instance.CurrentCamera.LookAt;

                    Vector3 newPos = lookAt - pos;
                    newPos.Normalize();
                    newPos = newPos * (float)(150.0f * gameTime.ElapsedGameTime.TotalSeconds);
                    GraphicsManager.Instance.CurrentCamera.Position = pos + newPos;
                    GraphicsManager.Instance.CurrentCamera.LookAt = lookAt + newPos;
                }
            }
            if (keyState.IsKeyDown(Keys.A))
            {
                if (Settings.USE_CHASECAM)
                {
                    (game as Game1).SceneManager.MovePlayer(Direction.Left);
                }
                else
                {
                    Vector3 pos2 = GraphicsManager.Instance.CurrentCamera.Position;
                    Vector3 lookAt2 = GraphicsManager.Instance.CurrentCamera.LookAt - pos2;
                    lookAt2.Normalize();

                    Vector3 axis = Vector3.Cross(lookAt2, Vector3.UnitY);
                    axis.Normalize();
                    axis = axis * (float)(150.0f * gameTime.ElapsedGameTime.TotalSeconds);
                    GraphicsManager.Instance.CurrentCamera.Position -= axis;
                    GraphicsManager.Instance.CurrentCamera.LookAt -= axis;
                }
            }
            if (keyState.IsKeyDown(Keys.S))
            {
                if (Settings.USE_CHASECAM)
                {
                    (game as Game1).SceneManager.MovePlayer(Direction.Down);
                }
                else
                {
                    Vector3 pos1 = GraphicsManager.Instance.CurrentCamera.Position;
                    Vector3 lookAt1 = GraphicsManager.Instance.CurrentCamera.LookAt;

                    Vector3 newPos1 = lookAt1 - pos1;
                    newPos1.Normalize();
                    newPos1 = newPos1 * (float)(150.0f * gameTime.ElapsedGameTime.TotalSeconds);
                    GraphicsManager.Instance.CurrentCamera.Position = pos1 - newPos1;
                    GraphicsManager.Instance.CurrentCamera.LookAt = lookAt1 - newPos1;
                }
            }
            if (keyState.IsKeyDown(Keys.D))
            {
                if (Settings.USE_CHASECAM)
                {
                    (game as Game1).SceneManager.MovePlayer(Direction.Right);
                }
                else
                {
                    Vector3 pos3 = GraphicsManager.Instance.CurrentCamera.Position;
                    Vector3 lookAt3 = GraphicsManager.Instance.CurrentCamera.LookAt - pos3;
                    lookAt3.Normalize();

                    Vector3 axis1 = Vector3.Cross(lookAt3, Vector3.UnitY);
                    axis1.Normalize();
                    axis1 = axis1 * (float)(150.0f * gameTime.ElapsedGameTime.TotalSeconds);
                    GraphicsManager.Instance.CurrentCamera.Position += axis1;
                    GraphicsManager.Instance.CurrentCamera.LookAt += axis1;
                }
            }

            oldState = keyState;
        }
        
        #endregion
    }
}
