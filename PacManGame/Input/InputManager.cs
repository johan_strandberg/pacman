﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace PacManGame.Input
{
    public class InputManager
    {
        static InputManager inputManager;
        //Lock for thread safety
        static readonly object syncRoot = new object();
        private List<IControl> ControlList = new List<IControl>();

        private InputManager()
        {
        }

        public static InputManager Instance
        {
            get
            {
                if (inputManager == null)
                {
                    lock (syncRoot)
                    {
                        if (inputManager == null)
                            inputManager = new InputManager();
                    }
                }
                return inputManager;
            }   

        }

        public void Initialize(Game game)
        {
            //**Keyboard input**
            Keyboard keyboard = new Keyboard();
            keyboard.Initialize(game);
            AddControl(keyboard);
            //*******************

            //****Mouse input****
            Mouse mouse = new Mouse();
            mouse.Initialize(game);
            AddControl(mouse);
            //*******************
        }

        public void Update(GameTime gameTime)
        {
            foreach (IControl control in ControlList)
                control.Update(gameTime);
        }

        public void AddControl(IControl control)
        {
            ControlList.Add(control);
        }
    }
}
