﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace PacManGame.Input
{
    public interface IControl
    {
        void Initialize(Game game);
        void Update(GameTime gameTime);
    }
}
