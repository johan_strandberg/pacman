﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace PacManGame.Input
{
    public class Mouse : IControl
    {
        #region IControl Members

        private MouseState oldState;
        private Game game;

        public void Initialize(Game game)
        {
            oldState = Microsoft.Xna.Framework.Input.Mouse.GetState();
            this.game = game;
        }

        public void Update(GameTime gameTime)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;
            MouseState mouseState = Microsoft.Xna.Framework.Input.Mouse.GetState();
            int mouseX = mouseState.X;
            int mouseY = mouseState.Y;

            if (mouseState.LeftButton == ButtonState.Pressed)
            {

            }
            if (mouseState.RightButton == ButtonState.Pressed)
            {

            }
            if (mouseState.MiddleButton == ButtonState.Pressed)
            {
                
            }
            if ((mouseState.X - oldState.X) > 0)
                GraphicsManager.Instance.CurrentCamera.YawAmount -= 1.0f/30.0f * dt * Math.Abs(mouseState.X - oldState.X);
            else if ((mouseState.X - oldState.X) < 0)
                GraphicsManager.Instance.CurrentCamera.YawAmount += 1.0f / 30.0f * dt * Math.Abs(mouseState.X - oldState.X);
            if ((mouseState.Y - oldState.Y) > 0)
                GraphicsManager.Instance.CurrentCamera.PitchAmount -= 1.0f / 30.0f * dt * Math.Abs(mouseState.Y - oldState.Y);
            else if ((mouseState.Y - oldState.Y) < 0)
                GraphicsManager.Instance.CurrentCamera.PitchAmount += 1.0f / 30.0f * dt * Math.Abs(mouseState.Y - oldState.Y);

            oldState = Microsoft.Xna.Framework.Input.Mouse.GetState();
        }

        #endregion
    }
}
