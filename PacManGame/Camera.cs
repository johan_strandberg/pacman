﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PacManGame
{
    public class Camera : MovableObject
    {
        private Vector3 lookAt;
        private Matrix viewMatrix;
        private Matrix projectionMatrix;
        private float aspectRatio;
        private BoundingFrustum frustum;
        private Vector3 cameraReference;
        private float yawAmount;
        private float forwardBackwardAmount;
        private float leftRightAmount;
        private float pitchAmount;
        private Vector3 up;
        private MovableObject target;
        private bool sideCam = false;

        public Camera(Viewport viewport, Vector3 pos, Vector3 lookat, Vector3 up)
        {
            this.up = up;
            this.aspectRatio = ((float)viewport.Width) / ((float)viewport.Height);
            this.projectionMatrix = Matrix.CreatePerspectiveFieldOfView(
                                        MathHelper.ToRadians(40.0f),
                                        this.aspectRatio,
                                        0.1f,
                                        10000.0f);
            this.lookAt = lookat;
            this.position = pos;
            UpdateDerivedMatrix();
            Matrix.CreateLookAt(ref pos, ref lookat, ref up, out viewMatrix);
            cameraReference = new Vector3(0, 0, -1);


            frustum = new BoundingFrustum(viewMatrix * projectionMatrix);
        }

        public Vector3 Up
        {
            get { return up; }
            set { up = value; }
        }

        public float YawAmount
        {
            get { return yawAmount; }
            set { yawAmount = value; }
        }

        public float PitchAmount
        {
            get { return pitchAmount; }
            set { pitchAmount = value; }
        }

        public float LeftRightAmount
        {
            get { return leftRightAmount; }
            set { leftRightAmount = value; }
        }

        public float ForwardBackwardAmount
        {
            get { return forwardBackwardAmount; }
            set { forwardBackwardAmount = value; }
        }

        public MovableObject Target
        {
            get { return target; }
            set { target = value; }
        }

        public Vector3 LookAt
        {
            get { return this.lookAt; }
            set { this.lookAt = value; }
        }

        public Matrix ViewMatrix
        {
            get { return this.viewMatrix; }
            set { this.viewMatrix = value; }
        }

        public Matrix ProjectionMatrix
        {
            get { return this.projectionMatrix; }
        }

        public BoundingFrustum BoundingFrustum
        {
            get { return frustum; }
        }

        public bool SideCam
        {
            get { return sideCam; }
            set { sideCam = value; }
        }

        public void Yaw(float degrees)
        {
            Vector3 look = lookAt - position;

            Matrix m = Matrix.CreateTranslation(look) * Matrix.CreateFromAxisAngle(Vector3.UnitY, degrees);
            lookAt = m.Translation + position;
        }

        public void Pitch(float degrees)
        {
            Vector3 look = lookAt - position;
            look.Normalize();
            Vector3 axis = Vector3.Cross(Vector3.UnitY, look);
            Matrix m = Matrix.CreateTranslation(look) * Matrix.CreateFromAxisAngle(axis, degrees);
            lookAt = m.Translation + position;
        }

        public void Update()
        {
            if (target != null)
            {
                Position = target.DerivedMatrix.Translation + new Vector3(15.0f, 30.0f, 0.0f);
            }
            if (NeedsUpdate)
                UpdateDerivedMatrix();



            //If there camera is following another SceneNode, set lookAt to its position
            if (target != null)
                lookAt = target.DerivedMatrix.Translation;

            // Set up the view matrix and projection matrix.
            viewMatrix = Matrix.CreateLookAt(position, lookAt, up);

            frustum.Matrix = viewMatrix * projectionMatrix;
        }

        public override void UpdateDerivedMatrix()
        {
            base.CalculateDerivedMatrix();

            this.NeedsUpdate = false;
        }

    }

}
