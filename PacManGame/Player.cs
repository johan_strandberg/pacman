﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PacManGame
{
    public class Player
    {
        private Entity controlledObject;
        private Vector2 moveDirection;
        private float moveSpeed;
        private int score = 0;
        private int lives = 3;
        private bool isMoving = false;
        private bool animateUp = false;
        private float animationAngle = (float)(Math.PI / 4.0);
        private ModelBone leftMouthBone;
        private ModelBone rightMouthBone;
        private Matrix leftMouthTransform;
        private Matrix rightMouthTransform;
        private bool isDead = false;

        public Player()
        {
            moveDirection = Vector2.Zero;
            moveSpeed = Settings.DEFAULT_MOVE_SPEED;
        }

        public bool IsDead
        {
            get { return isDead; }
            set { isDead = value; }
        }

        public bool IsMoving
        {
            get { return isMoving; }
            set { isMoving = value; }
        }

        public bool AnimateUp
        {
            get { return animateUp; }
            set { animateUp = value; }
        }

        public float AnimationAngle
        {
            get { return animationAngle; }
            set { animationAngle = value; }
        }

        public int Score
        {
            get { return score; }
            set { score = value; }
        }

        public int Lives
        {
            get { return lives; }
            set { lives = value; }
        }

        public Entity ControlledObject
        {
            get { return controlledObject; }
            set 
            {
                controlledObject = value;
                leftMouthBone = controlledObject.Model.Meshes[1].ParentBone;
                rightMouthBone = controlledObject.Model.Meshes[2].ParentBone;
                leftMouthTransform = leftMouthBone.Transform;
                rightMouthTransform = rightMouthBone.Transform;
            }
        }

        public Vector2 MoveDirection
        {
            get { return moveDirection; }
            set { moveDirection = value; }
        }

        public float MoveSpeed
        {
            get { return moveSpeed; }
            set { moveSpeed = value; }
        }

        public void MoveLeft()
        {
            moveDirection.X = -1;
            moveDirection.Y = 0;
            controlledObject.Rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, (float)Math.PI);
        }

        public void MoveRight()
        {
            moveDirection.X = 1;
            moveDirection.Y = 0;
            controlledObject.Rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, 0);
        }

        public void MoveForward()
        {
            moveDirection.X = 0;
            moveDirection.Y = -1;
            controlledObject.Rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, (float)(Math.PI / 2.0));
        }

        public void MoveBackward()
        {
            moveDirection.X = 0;
            moveDirection.Y = 1;
            controlledObject.Rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, -(float)(Math.PI / 2.0));
        }

        public void Draw()
        {
            Model m = controlledObject.Model;

            Matrix[] boneTransforms = new Matrix[m.Bones.Count];

            Matrix leftMouthRotation = Matrix.CreateRotationZ(-animationAngle);
            Matrix rightMouthRotation = Matrix.CreateRotationZ(animationAngle);

            leftMouthBone.Transform = leftMouthRotation * leftMouthTransform;
            rightMouthBone.Transform = rightMouthRotation * rightMouthTransform;

            m.CopyAbsoluteBoneTransformsTo(boneTransforms);

            foreach (ModelMesh mesh in m.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.Parameters["World"].SetValue(boneTransforms[mesh.ParentBone.Index] * ControlledObject.DerivedMatrix);
                    effect.Parameters["View"].SetValue(GraphicsManager.Instance.CurrentCamera.ViewMatrix);
                    effect.Parameters["Projection"].SetValue(GraphicsManager.Instance.CurrentCamera.ProjectionMatrix);
                }
                mesh.Draw();
            }
        }
    }
}
