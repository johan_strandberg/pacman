﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PacManGame
{
    class Enemy
    {
        private Entity controlledObject;
        private Vector2 moveDirection;
        private float moveSpeed;
        private Direction currentDirection = Direction.None;
        private bool isMoving = false;
        private Vector2 lastJunction = new Vector2(-1, -1);
        Random r = new Random(DateTime.Now.Millisecond);
        private double offset;
        private float frightenedTime = 0.0f;
        private bool eaten = false;
        private Color defaultColor = Color.White;

        public Enemy()
        {
            moveDirection = Vector2.Zero;
            moveSpeed = Settings.DEFAULT_MOVE_SPEED;
            offset = r.Next(360);
            offset = offset * Math.PI / 180.0;
        }

        public Vector2 LastJunction
        {
            get { return lastJunction; }
            set { lastJunction = value; }
        }

        public bool Frightened
        {
            get { return frightenedTime > 0.0f; }
            set
            {
                if (value)
                {
                    frightenedTime = 10.0f;
                    moveSpeed = 3.0f;
                    controlledObject.Color = Color.DarkBlue;
                }
                else
                {
                    frightenedTime = 0.0f;
                    moveSpeed = Settings.DEFAULT_MOVE_SPEED;
                    controlledObject.Color = defaultColor;
                }
            }
        }

        public Color DefaultColor
        {
            get { return defaultColor; }
            set 
            { 
                defaultColor = value;
                if (controlledObject != null)
                    controlledObject.Color = value;
            }
        }

        public bool Eaten
        {
            get { return eaten; }
            set { eaten = value; }
        }

        public bool IsMoving
        {
            get { return isMoving; }
            set { isMoving = value; }
        }

        public Vector2 MoveDirection
        {
            get { return moveDirection; }
            set { moveDirection = value; }
        }

        public float MoveSpeed
        {
            get { return moveSpeed; }
            set { moveSpeed = value; }
        }

        public Entity ControlledObject
        {
            get { return controlledObject; }
            set { controlledObject = value; }
        }

        private void Move(Direction dir)
        {
            switch(dir)
            {
                case Direction.Down:
                    MoveBackward();
                    break;
                case Direction.Up:
                    MoveForward();
                    break;
                case Direction.Left:
                    MoveLeft();
                    break;
                case Direction.Right:
                    MoveRight();
                    break;
            }
        }

        public void MoveLeft()
        {
            moveDirection.X = -1;
            moveDirection.Y = 0;
            UpdateRotation();
            currentDirection = Direction.Left;
        }

        public void MoveRight()
        {
            moveDirection.X = 1;
            moveDirection.Y = 0;
            UpdateRotation();
            currentDirection = Direction.Right;
        }

        public void MoveForward()
        {
            moveDirection.X = 0;
            moveDirection.Y = -1;
            UpdateRotation();
            currentDirection = Direction.Up;
        }

        public void MoveBackward()
        {
            moveDirection.X = 0;
            moveDirection.Y = 1;
            UpdateRotation();
            currentDirection = Direction.Down;
        }

        private void UpdateRotation()
        {
            controlledObject.Rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, (float)Math.Asin(moveDirection.Y) + (float)Math.Acos(moveDirection.X));
        }

        public void Update(GameTime gameTime)
        {
            controlledObject.Position = new Vector3(controlledObject.Position.X, 0.25f + (float)Math.Sin(gameTime.TotalGameTime.TotalSeconds * 8 + offset) / 4.0f, controlledObject.Position.Z);

            if (Frightened)
            {
                if (frightenedTime > 2.0f)
                    controlledObject.Color = Color.Lerp(Settings.GHOST_FRIGHTENED_COLOR, Color.Blue, 0.25f + (float)Math.Sin(gameTime.TotalGameTime.TotalSeconds * 8));
                else
                    controlledObject.Color = Color.Lerp(Settings.GHOST_FRIGHTENED_COLOR, Color.Blue, 0.25f + (float)Math.Sin(gameTime.TotalGameTime.TotalSeconds * 16));

                frightenedTime -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (!Frightened)
                    Frightened = false;
            }
        }

        public void UpdateDirection(int x, int y)
        {
            while (true)
            {
                Direction next = (Direction)(r.Next(4) + 1);
                if (next != SceneManager.Instance.Opposite(currentDirection))
                {
                    if (!CheckWallCollision(x, y, next))
                    {
                        Move(next);
                        lastJunction.X = x;
                        lastJunction.Y = y;
                        break;
                    }
                }
            }
        }

        private bool CheckWallCollision(int x, int y, Direction next)
        {
            switch (next)
            {
                case Direction.Up:
                    return SceneManager.Instance.CheckWallCollision(x, y - 1);
                case Direction.Down:
                    return SceneManager.Instance.CheckWallCollision(x, y + 1);
                case Direction.Left:
                    return SceneManager.Instance.CheckWallCollision(x - 1, y);
                case Direction.Right:
                    return SceneManager.Instance.CheckWallCollision(x + 1, y);
                default:
                    return false;
            }
        }
    }
}
