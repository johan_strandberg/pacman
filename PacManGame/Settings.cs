﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PacManGame
{
    public static class Settings
    {
        public static bool DEBUG_RENDERING = false;
        public static bool USE_CHASECAM = true;
        public static bool USE_SKYBOX = false;
        public static float DEFAULT_MOVE_SPEED = 4.5f;
        public static Vector3 LIGHT_DIRECTION = new Vector3(0, -1, 0);
        public static float AMBIENT_INTENSITY = 0.5f;
        public static Color GHOST_FRIGHTENED_COLOR = Color.DarkBlue;
    }
}
