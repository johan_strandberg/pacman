﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PacManGame.ScreenManager;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace PacManGame.Screens
{
    class TitleScreen : GameScreen
    {
        Texture2D image;
        double timeToLive;
        double lifeSpan;
        float alpha = 0.0f;

        public TitleScreen()
        {
            image = ResourceLoader.Instance.loadTexture2D("intro");
            timeToLive = lifeSpan = 5.0;
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            timeToLive -= gameTime.ElapsedGameTime.TotalSeconds;
            if (timeToLive < 0)
            {
                GotoMainMenu();
            }
        }

        private void GotoMainMenu()
        {
            timeToLive = 1000.0;
            ExitScreen();
            this.ScreenManager.AddScreen(new MainMenuScreen(), null);
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            // Make the menu slide into place during transitions, using a
            // power curve to make things look more interesting (this makes
            // the movement slow down as it nears the end).
            double temp = 1 - (timeToLive / lifeSpan) * 2;
            alpha = 1.0f - (float)Math.Pow(Math.Abs(temp), 2);
            Color color = new Color(1.0f, 1.0f, 1.0f, alpha);

            Vector2 imagepos = new Vector2();
            imagepos.X = this.ScreenManager.GraphicsDevice.PresentationParameters.BackBufferWidth / 2;
            imagepos.Y = this.ScreenManager.GraphicsDevice.PresentationParameters.BackBufferHeight / 2;
            imagepos.X -= image.Width / 2;
            imagepos.Y -= image.Height / 2;

            Vector2 textpos = new Vector2(imagepos.X, imagepos.Y + image.Height + 10);

            spriteBatch.Begin();
            
            spriteBatch.Draw(image, imagepos, color);
            spriteBatch.DrawString(this.ScreenManager.Font, "Made by me!", textpos, color, 0.0f, Vector2.Zero, 1.5f, SpriteEffects.None, 0.0f);

            spriteBatch.End();
        }

        public override void HandleInput(InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            PlayerIndex pi;

            if (input.IsMenuSelect(ControllingPlayer, out pi))
            {
                GotoMainMenu();
            }
            if (input.IsMenuCancel(ControllingPlayer, out pi))
            {
                GotoMainMenu();
            }
        }
    }
}
