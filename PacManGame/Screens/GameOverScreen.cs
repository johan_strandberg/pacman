﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PacManGame.ScreenManager;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.IO;

namespace PacManGame.Screens
{
    class GameOverScreen : GameScreen
    {
        string name = "";

        public GameOverScreen() : base()
        {
            IsPopup = true;
        }

        private void GotoMainMenu()
        {
            LoadingScreen.Load(ScreenManager, false, null, new MainMenuScreen());
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;
            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);

            spriteBatch.Begin();

            Vector2 center = new Vector2();
            center.X = this.ScreenManager.GraphicsDevice.PresentationParameters.BackBufferWidth / 2;
            center.Y = this.ScreenManager.GraphicsDevice.PresentationParameters.BackBufferHeight / 2;

            Vector2 namepos = center;
            Vector2 measurements = font.MeasureString("Name: ");
            namepos.X -= measurements.X;
            namepos.Y -= measurements.Y / 2 - 40;

            spriteBatch.DrawString(font, "Name: ", namepos, Color.Gold);
            spriteBatch.DrawString(font, name, new Vector2(measurements.X, 0) + namepos, Color.Cornsilk);

            Vector2 gameoverpos = center;
            measurements = font.MeasureString("Game Over!");
            measurements = measurements * 1.5f;
            gameoverpos.X -= measurements.X / 2;
            gameoverpos.Y -= measurements.Y / 2;

            Vector2 scorepos = center;
            measurements = font.MeasureString("Final Score: ");
            scorepos.X -= measurements.X;
            scorepos.Y -= measurements.Y / 2 - 20;

            spriteBatch.DrawString(font, "Game Over!", gameoverpos, Color.Gold);
            spriteBatch.DrawString(font, "Final Score: ", scorepos, Color.Gold);
            spriteBatch.DrawString(font, SceneManager.Instance.Player.Score.ToString(), new Vector2(measurements.X, 0) + scorepos, Color.Cornsilk);

            spriteBatch.End();
        }

        public override void HandleInput(InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            PlayerIndex pi;
            if (input.IsMenuSelect(ControllingPlayer, out pi))
            {
                SaveScore();
                GotoMainMenu();
            }
            if (input.IsMenuCancel(ControllingPlayer, out pi))
            {
                GotoMainMenu();
            }

            Keys[] pressedKeys = input.CurrentKeyboardStates[0].GetPressedKeys();

            foreach (Keys key in pressedKeys)
            {
                if (input.LastKeyboardStates[0].IsKeyUp(key))
                {
                    if (key == Keys.Space)
                        name = name.Length < 16 ? name + " " : name;
                    else if (key == Keys.Back)
                        name = name.Length > 0 ? name.Substring(0, name.Length - 1) : name;
                    else if (key.ToString().Length == 1)
                        name = name.Length < 16 ? name + key.ToString() : name;
                }
            }
        }

        private void SaveScore()
        {
            List<string> entries = new List<string>();
            if (!File.Exists("highscores.txt"))
            {
                FileStream fs = File.Create("highscores.txt");
                fs.Close();
            }
            else
            {
                StreamReader sr = File.OpenText("highscores.txt");
                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();
                    string[] split = s.Split(':');
                    bool inserted = false;

                    for (int i = 0; i < entries.Count; i++)
                    {
                        if (int.Parse(split[0]) >= int.Parse(entries[i].Split(':')[0]))
                        {
                            entries.Insert(i, s);
                            inserted = true;
                            break;
                        }
                    }

                    if (!inserted)
                        entries.Add(s);
                }
                sr.Close();
            }

            string score = SceneManager.Instance.Player.Score.ToString() + ":" + name;
            bool ins = false;

            for (int i = 0; i < entries.Count; i++)
            {
                if (SceneManager.Instance.Player.Score >= int.Parse(entries[i].Split(':')[0]))
                {
                    entries.Insert(i, score);
                    ins = true;
                    break;
                }
            }

            if (!ins)
                entries.Add(score);

            if (File.Exists("highscores.txt"))
                File.Delete("highscores.txt");

            StreamWriter writer = File.CreateText("highscores.txt");
            foreach (string str in entries)
            {
                writer.WriteLine(str);
            }
            writer.Flush();
            writer.Close();
        }
    }
}
