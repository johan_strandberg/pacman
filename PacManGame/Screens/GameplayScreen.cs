#region Using Statements
using System;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PacManGame.ScreenManager;
using PacManGame.Graphics;
#endregion

namespace PacManGame.Screens
{
    class GameplayScreen : GameScreen
    {
        #region Fields

        ContentManager content;
        SpriteFont gameFont;
        Random random = new Random();
        bool started = false;
        float elapsedTime = 0.0f;
        Texture2D pacIcon;
        ReflectionPlane plane;
        float fruitTimer;

        #endregion

        #region Initialization


        public GameplayScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
            SceneManager.Instance.Initialize();
        }

        public bool Started
        {
            get { return started; }
            set
            {
                if (value && !started)
                    SoundManager.Instance.PlayMusic();
                else if (!value)
                    SoundManager.Instance.StopMusic();

                started = value;
            }
        }

        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");

            gameFont = ResourceLoader.Instance.loadFont("font1");
            pacIcon = ResourceLoader.Instance.loadTexture2D("pac-man-small");
            SceneManager.Instance.LoadContent();

            plane = new ReflectionPlane(this.ScreenManager.Game);

            // once the load has finished, we use ResetElapsedTime to tell the game's
            // timing mechanism that we have just finished a very long frame, and that
            // it should not try to catch up.
            ScreenManager.Game.ResetElapsedTime();

            fruitTimer = random.Next(10, 40);
        }


        public override void UnloadContent()
        {
            content.Unload();
        }


        #endregion

        #region Update and Draw

        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            elapsedTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (IsActive && started)
            {
                if (fruitTimer < elapsedTime)
                {
                    fruitTimer += 10.0f;
                    fruitTimer += random.Next(10, 40);
                    SceneManager.Instance.Fruitactive = 10.0f;
                }

                SceneManager.Instance.Update(gameTime);

                if (SceneManager.Instance.GameFinished)
                {
                    SceneManager.Instance.NextLevel();
                    ScreenManager.Game.ResetElapsedTime();
                    elapsedTime = 0.0f;
                    Started = false;
                }
                else if (SceneManager.Instance.Player.IsDead)
                {
                    if (SceneManager.Instance.Player.Lives > 0)
                    {
                        SceneManager.Instance.ResetPositions();
                        SceneManager.Instance.Player.IsDead = false;
                        elapsedTime = 0.0f;
                        Started = false;
                    }
                    else
                    {
                        SoundManager.Instance.StopMusic();
                        this.ScreenManager.AddScreen(new GameOverScreen(), null);
                    }
                }
            }
        }

        public override void HandleInput(InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            // Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;

            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];

            // The game pauses either if the user presses the pause button, or if
            // they unplug the active gamepad. This requires us to keep track of
            // whether a gamepad was ever plugged in, because we don't want to pause
            // on PC if they are playing with a keyboard and have no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected &&
                                       input.GamePadWasConnected[playerIndex];

            if (input.IsPauseGame(ControllingPlayer) || gamePadDisconnected)
            {
                SoundManager.Instance.PauseMusic();
                ScreenManager.AddScreen(new PauseMenuScreen(), ControllingPlayer);
            }
            else if (started)
            {
                if (keyboardState.IsKeyDown(Keys.Left))
                    SceneManager.Instance.MovePlayer(Direction.Left);

                if (keyboardState.IsKeyDown(Keys.Right))
                    SceneManager.Instance.MovePlayer(Direction.Right);

                if (keyboardState.IsKeyDown(Keys.Up))
                    SceneManager.Instance.MovePlayer(Direction.Up);

                if (keyboardState.IsKeyDown(Keys.Down))
                    SceneManager.Instance.MovePlayer(Direction.Down);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            plane.Draw(gameTime);

            GraphicsManager.Instance.Begin();
            SceneManager.Instance.Draw(gameTime);
            GraphicsManager.Instance.Finish();

            //Draw ready, set, go strings.
            if (elapsedTime < 5.5f)
                CountDown(gameTime);

            DrawScore();


            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0)
                ScreenManager.FadeBackBufferToBlack(255 - TransitionAlpha);
        }

        private void DrawScore()
        {
            Viewport viewport = this.ScreenManager.GraphicsDevice.Viewport;
            SpriteBatch sb = this.ScreenManager.SpriteBatch;

            sb.Begin();
            sb.DrawString(gameFont, "Score: " + SceneManager.Instance.Player.Score, new Vector2(10.0f), Color.White);

            Vector2 temp = new Vector2(200.0f, 10.0f);
            sb.DrawString(gameFont, "Lives: ", temp, Color.White);
            temp.X += 70.0f;
            temp.Y -= 5.0f;

            for (int i = 0; i < SceneManager.Instance.Player.Lives; i++)
            {
                temp.X += 25.0f;
                sb.Draw(pacIcon, temp, Color.White);
            }

            sb.End();
        }

        private void CountDown(GameTime gameTime)
        {
            Viewport viewport = this.ScreenManager.GraphicsDevice.Viewport;
            SpriteBatch sb = this.ScreenManager.SpriteBatch;

            sb.Begin();

            Vector2 measure = gameFont.MeasureString("Level: 0");
            sb.DrawString(gameFont, "Level:", new Vector2(viewport.Width / 2 - measure.X / 2, 230), Color.Gold);
            sb.DrawString(gameFont, SceneManager.Instance.LevelNumber.ToString(), new Vector2(viewport.Width / 2 + measure.X / 2 - 20, 230), Color.Cornsilk);

            if (elapsedTime < 2.5f)
            {
                float alpha = 1.0f - (float)(elapsedTime - 1.0) / 2.0f;
                Color color = new Color(1.0f, 1.0f, 1.0f, alpha);
                Vector2 stringpos = gameFont.MeasureString("Ready!");
                sb.DrawString(gameFont, "Ready!", new Vector2(viewport.Width / 2 - stringpos.X / 2, viewport.Height / 2 - stringpos.Y / 2), color);
            }
            else if (elapsedTime < 4.0f)
            {
                float alpha = 1.0f - (float)(elapsedTime - 2.5) / 2.0f;
                Color color = new Color(1.0f, 1.0f, 1.0f, alpha);
                Vector2 stringpos = gameFont.MeasureString("Set!");
                sb.DrawString(gameFont, "Set!", new Vector2(viewport.Width / 2 - stringpos.X / 2, viewport.Height / 2 - stringpos.Y / 2), color);
            }
            else if (elapsedTime < 5.5f)
            {
                float alpha = 1.0f - (float)(elapsedTime - 4.0) / 2.0f;
                Color color = new Color(1.0f, 1.0f, 1.0f, alpha);
                Vector2 stringpos = gameFont.MeasureString("Go!");
                sb.DrawString(gameFont, "Go!", new Vector2(viewport.Width / 2 - stringpos.X / 2, viewport.Height / 2 - stringpos.Y / 2), color);
                Started = true;
            }
            sb.End();
        }

        #endregion
    }
}
