﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PacManGame.ScreenManager;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace PacManGame.Screens
{
    class HighScoreScreen : GameScreen
    {
        string name = "";
        List<string> entries = new List<string>();

        public HighScoreScreen() : base()
        {
            IsPopup = true;
            if (File.Exists("highscores.txt"))
            {
                StreamReader fs = File.OpenText("highscores.txt");
                while(!fs.EndOfStream)
                    entries.Add(fs.ReadLine());
                fs.Close();
            }

            TransitionOnTime = TimeSpan.FromSeconds(0.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }

        private void GotoMainMenu()
        {
            LoadingScreen.Load(ScreenManager, false, null, new MainMenuScreen());
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;
            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 1);
            Color color1 = Color.Gold;
            color1.A = TransitionAlpha;
            Color color2 = Color.Cornsilk;
            color2.A = TransitionAlpha;
            Color color3 = Color.Aqua;
            color3.A = TransitionAlpha;

            spriteBatch.Begin();
            
            Vector2 pos = new Vector2(125, 150);

            spriteBatch.DrawString(font, "Highscores:", pos, color1);
            pos.Y += 15;

            foreach (string s in entries)
            {
                pos.Y += 20;
                string[] split = s.Split(':');
                spriteBatch.DrawString(font, split[1], pos, color2);
                spriteBatch.DrawString(font, split[0], new Vector2(250, 0) + pos, color3);
            }

            spriteBatch.End();
        }

        public override void HandleInput(InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            PlayerIndex pi;
            if (input.IsMenuSelect(ControllingPlayer, out pi))
            {
                ExitScreen();
            }
            if (input.IsMenuCancel(ControllingPlayer, out pi))
            {
                ExitScreen();
            }
        }
    }
}
