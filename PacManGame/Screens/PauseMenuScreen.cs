#region Using Statements
using Microsoft.Xna.Framework;
#endregion

namespace PacManGame.Screens
{
    class PauseMenuScreen : MenuScreen
    {
        #region Initialization
        public PauseMenuScreen()
            : base("Paused")
        {
            // Flag that there is no need for the game to transition
            // off when the pause menu is on top of it.
            IsPopup = true;
            BackgroundVisible = false;

            // Create our menu entries.
            MenuEntry resumeGameMenuEntry = new MenuEntry("Resume Game");
            MenuEntry quitGameMenuEntry = new MenuEntry("Quit Game");

            // Hook up menu event handlers.
            resumeGameMenuEntry.Selected += OnCancel;
            quitGameMenuEntry.Selected += QuitGameMenuEntrySelected;

            // Add entries to the menu.
            MenuEntries.Add(resumeGameMenuEntry);
            MenuEntries.Add(quitGameMenuEntry);
        }


        #endregion

        #region Handle Input
        void QuitGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            SoundManager.Instance.StopMusic();
            LoadingScreen.Load(ScreenManager, false, null, new MainMenuScreen());
        }

        void ConfirmQuitMessageBoxAccepted(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, false, null, new MainMenuScreen());
        }


        #endregion

        #region Draw
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.FadeBackBufferToBlack(TransitionAlpha * 2 / 3);
            //base.DrawBackground(((float)TransitionAlpha/255.0f) * 2.0f / 3.0f);

            base.Draw(gameTime);
        }


        #endregion
    }
}
